<?php 

#$string = "Fore Sure, Vor Nach";
#$string = "Sure, Fore; Nach, Vor";
#print transformer_operator_normalizeauthors_execute($string);
	#preg_match_all("/([A-Z][a-z]+), /", $string, $matches);
	#print $matches; print_r($matches); die("done");
	#foreach($matches[1] AS $key => $value) { print "auth: $value"; 

function transformer_operator_normalizeauthors_name() {
	return "normalize author names";
}


#function atomizeauthors($string) {
function transformer_operator_normalizeauthors_execute($string) {
	#drupal_set_message("op on $string");
	if ($string=="") return;

	# s/\([^)]*\)//g; # remove any prens
	$string = preg_replace("/\([^)]*\)/", "", $string);
	# s/\n//g;
	$string = preg_replace("/\n/", "", $string);
	# s/\r//g;
	$string = preg_replace("/\r/", "", $string);
	# s/[ ]+/ /g;
	$string = preg_replace("/[ ]+/", " ", $string);
	# s/^ //;
	$string = preg_replace("/^ /", "", $string);
	# s/ $//;
	$string = preg_replace("/[ ,]$/", "", $string);
	$string = preg_replace("/([^A-Z])\.$/", "$1", $string);
	return extaut($string);
}




# atomize single authors from multiple authors in one line
function extaut($authors) {
	$did = "1 ";
#  print "-------------\n".$authors."\n";
  $splitregexp = "/[,; ]* and  */";
#  if ($authors=~/;/) {
#  if preg_match( "/(\w+)/", $authors, $match );
  if ( strpos($authors, ";")) {
	  #$splitregexp="/ *; */";
	  $splitregexp="/([,; ]* and  *)|( *; *)/";
	  $did.="2 ";
  }
  # line not having ';' nor 'and' but be of form 'Xxx Xxx, Xxx Xxx' with Xxx just not being [space]
#  if ($authors!~/;/ and $authors!~/ and / and $authors=~/[^ ]+ +[^ ]+, *[^ ]+ +[^ ]+/) {
  if (!strpos($authors, ";") /*and !strpos($authors, " and ")*/ and preg_match("/[^ ]+ +[^ ]+ *(,| and ) *[^ ]+ +[^ ]+/", $authors)) {
  	  #$splitregexp="/ *, */";
	  $splitregexp="/([,; ]* and  *)|( *, *)/";
	  $did.="3 ";
  }
  $authorsplit = preg_split($splitregexp, $authors); #default split by ' and ': if not avail.: no split(?)
  #print length(@authorsplit);
  #print_r($authorsplit);
  $authornames = array();
  $i=0;
  #foreach($authorsplit as $anauthor) {
  while ($i<count($authorsplit)) {
    ## split too much?
	### part only having "X." or "X"  or part not having [ ,.] or not having [a-z] cant be full authorname !!
    #if ( ($authorsplit[$i]=~/^.\.?$/ ) or ( $authorsplit[$i]!~/[ ,.]/ ) or ( $authorsplit[$i]!~/[a-z]/ ) ) {
	if ( preg_match("/^ *.\.? *$/", $authorsplit[$i+1]) or !preg_match("/[ ,.]/", $authorsplit[$i+1]) or !preg_match("/[a-z]/", $authorsplit[$i+1]) ) {
      $authornames[] = isset($authorsplit[$i+1]) ? $authorsplit[$i].", ".$authorsplit[$i+1] : $authorsplit[$i];
	  $did.="4 ";
	  $i++; //?
	  #next;next;
    } else {
      $authornames[] = $authorsplit[$i];
	  #$i++;
	  $did.="5 ";
    }
    $i++;
  }

  $authorconcat;
  $k=0;
  foreach($authornames as $authorname) {
    #print $k+1;
	#print ": ";
    #print $authornames[$k]."\n";
    $k++;
	## extract sure/forenames
#	$authorconcat.= $authorname."; ";
	$authorconcat.= foresur($authorname)."; ";
  }
#  $authorconcat=~s/; $//;
  $authorconcat = preg_replace("/; $/", "", $authorconcat);
  return $authorconcat;
}



# extract surname and rest=forename(s) from single author
function foresur($author) {
	$surname;
	$forenames;
	
#	if ($author=~/,/) {  
	if (strpos($author, ",")) {
		##  Surname, For Names
		#$author=~/([^,]+), *(.*)/;
		preg_match("/([^,]+), *(.*)/", $author, $matches);
		$surname = $matches[1];
		$forenames = $matches[2];
	} else {
		# For Names Surname  (doesnt catch "van Names")
		#$author=~/(.*) ([^ ]+)/;
		#$surname = $2;
		#$forenames = $1;

		preg_match("/ ((van )?[^ ]+)$/", $author, $matches);
		#$author=~/ ((van )?[^ ]+)$/;
		$surname = $matches[1];
		#$forenames = substr($author, 0, length($author)-length($surname));
		$forenames = substr($author, 0, strlen($author)-strlen($surname));

	}
	#print "** $surname **, $forenames\n";
	#$forenames=~s/^ //;
	#$forenames=~s/ $//;

	# s/^ //;
	$forenames = preg_replace("/^ /", "", $forenames);
	# s/ $//;
	$forenames = preg_replace("/ $/", "", $forenames);
	return "$surname, $forenames";
}


?>
